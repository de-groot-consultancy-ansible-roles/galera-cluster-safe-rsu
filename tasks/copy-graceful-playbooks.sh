#!/bin/bash

cp ../../mariadb-mysql-galera/tasks/graceful-proxy-start.yml .
cp ../../mariadb-mysql-galera/tasks/graceful-proxy-stop.yml .

sed -i 's/mariadb_login_user/mysql_login_user/g' graceful-proxy-start.yml
sed -i 's/mariadb_login_password/mysql_login_password/g' graceful-proxy-start.yml
sed -i 's/mariadb_login_host/mysql_login_host/g' graceful-proxy-start.yml
sed -i 's/mariadb_login_port/mysql_login_port/g' graceful-proxy-start.yml
sed -i 's/mariadb_login_unix_socket/mysql_login_unix_socket/g' graceful-proxy-start.yml

sed -i 's/mariadb_graceful_switchover_default_wait_time/rsu_graceful_switchover_default_wait_time/g' graceful-proxy-start.yml
sed -i 's/mariadb_graceful_switchover_proxysql_hosts/rsu_graceful_switchover_proxysql_hosts/g' graceful-proxy-start.yml
sed -i 's/mariadb_graceful_switchover_extra_wait_time/rsu_graceful_switchover_extra_wait_time/g' graceful-proxy-start.yml
sed -i 's/mariadb_graceful_switchover_extra_wait_users/rsu_graceful_switchover_extra_wait_users/g' graceful-proxy-start.yml
sed -i 's/mariadb_graceful_switchover_proxy_node_address/rsu_graceful_switchover_proxy_node_address/g' graceful-proxy-start.yml
sed -i 's/mariadb_graceful_switchover_fallback_failover/rsu_graceful_switchover_fallback_failover/g' graceful-proxy-start.yml

sed -i 's/mariadb_login_user/mysql_login_user/g' graceful-proxy-stop.yml
sed -i 's/mariadb_login_password/mysql_login_password/g' graceful-proxy-stop.yml
sed -i 's/mariadb_login_host/mysql_login_host/g' graceful-proxy-stop.yml
sed -i 's/mariadb_login_port/mysql_login_port/g' graceful-proxy-stop.yml
sed -i 's/mariadb_login_unix_socket/mysql_login_unix_socket/g' graceful-proxy-stop.yml

sed -i 's/mariadb_graceful_switchover_default_wait_time/rsu_graceful_switchover_default_wait_time/g' graceful-proxy-stop.yml
sed -i 's/mariadb_graceful_switchover_proxysql_hosts/rsu_graceful_switchover_proxysql_hosts/g' graceful-proxy-stop.yml
sed -i 's/mariadb_graceful_switchover_extra_wait_time/rsu_graceful_switchover_extra_wait_time/g' graceful-proxy-stop.yml
sed -i 's/mariadb_graceful_switchover_extra_wait_users/rsu_graceful_switchover_extra_wait_users/g' graceful-proxy-stop.yml
sed -i 's/mariadb_graceful_switchover_proxy_node_address/rsu_graceful_switchover_proxy_node_address/g' graceful-proxy-stop.yml
sed -i 's/mariadb_graceful_switchover_fallback_failover/rsu_graceful_switchover_fallback_failover/g' graceful-proxy-stop.yml
